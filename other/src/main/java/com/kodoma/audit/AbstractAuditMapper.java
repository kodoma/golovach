package com.kodoma.audit;

import java.io.Serializable;
import java.util.List;

/**
 * Абстрактеный маппер.
 */
public abstract class AbstractAuditMapper {

    public void fillParameters(final List<Serializable> list) {
        fillInputParameters(list);
        fillOutputParameters(list);
    }

    abstract void fillInputParameters(final List<Serializable> list);

    abstract void fillOutputParameters(final List<Serializable> list);

}
