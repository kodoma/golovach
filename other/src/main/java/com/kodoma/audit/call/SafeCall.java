package com.kodoma.audit.call;

/**
 *
 */
public class SafeCall {

    private static final Holder HOLDER = new Holder();

     public <V> V getValue(V value) {
         if (value == null) {
             return null;
         }
         HOLDER.putValue(value);
         return value;
     }

}
