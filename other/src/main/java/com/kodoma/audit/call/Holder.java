package com.kodoma.audit.call;

/**
 *
 */
public class Holder<V> {

    private V value;

    public void putValue(V value) {
        this.value = value;
    }

    public V getValue() {
        return value;
    }
}
