package com.kodoma.audit.call;

import java.util.function.Supplier;

public class SomeClass {

    public static void main(String[] args) {
        String s = null;

        String opt = opt(() -> s.getBytes().getClass().getSimpleName());

        System.out.println(opt);
    }

    public static <T> T opt(Supplier<T> statement) {
        try {
            return statement.get();
        } catch (NullPointerException exc) {
            return null;
        }
    }
}
