package com.kodoma;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Простая коллекция
 */
public class SimpleHolder {

    /** Первый элемент. */
    private static final ValueHolder<Serializable> FIRST = new ValueHolder<Serializable>();

    /** Второй элемент. */
    private static final ValueHolder<Serializable> SECOND = new ValueHolder<Serializable>();

    /** Третий элемент. */
    private static final ValueHolder<Serializable> THIRD = new ValueHolder<Serializable>();

    /** Четвертый элемент. */
    private static final ValueHolder<Serializable> FOURTH = new ValueHolder<Serializable>();

    /** Внутренний счетчик */
    private static int count = 1;

    private static final Map<Integer, ValueHolder<Serializable>> MAP = new HashMap<Integer, ValueHolder<Serializable>>();

    static {
        MAP.put(1, FIRST);
        MAP.put(2, SECOND);
        MAP.put(3, THIRD);
        MAP.put(4, FOURTH);
    }

    private static class ValueHolder<V> {

        private V value;

        public V getValue() {
            return value;
        }

        void setValue(V value) {
            this.value = value;
        }
    }

    public SimpleHolder putValue(Serializable value) {
        ValueHolder<Serializable> v = getValue(count++);
        v.setValue(value);
        return this;
    }

    public Serializable getFirst() {
        return FIRST.getValue();
    }

    public Serializable getSecond() {
        return SECOND.getValue();
    }

    public Serializable getThird() {
        return THIRD.getValue();
    }

    public Serializable getFourth() {
        return FOURTH.getValue();
    }

    private static ValueHolder<Serializable> getValue(final Integer num) {
        return MAP.get(num);
    }

}
