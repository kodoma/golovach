package com.kodoma.callback.with_values;

import javax.swing.*;

/**
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class SomeClass {

    interface Callback {

        void callingBack(String s);
    }

    private Callback callback;

    public void registerCallBack(Callback callback) {
        this.callback = callback;
    }

    void doSomething() {
        final int reply = JOptionPane.showConfirmDialog(null, "Вы программист?", "Опрос", JOptionPane.YES_NO_OPTION);
        callback.callingBack(reply == JOptionPane.YES_OPTION ? "Да" : "Нет");
    }
}