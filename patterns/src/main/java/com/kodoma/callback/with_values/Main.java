package com.kodoma.callback.with_values;

/**
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class Main {

    public static void main(String[] args) {
        final SomeClass someClass = new SomeClass();
        final MyClass myClass = new MyClass();

        someClass.registerCallBack(myClass);
        someClass.doSomething();
    }
}
