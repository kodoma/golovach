package com.kodoma.callback.with_values;

/**
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class MyClass implements SomeClass.Callback {

    @Override
    public void callingBack(String s) {
        if (s != null) {
            System.out.println("Ваш ответ: " + s);
        } else {
            System.out.println("Вы не ответили на вопрос!");
        }
    }
}
