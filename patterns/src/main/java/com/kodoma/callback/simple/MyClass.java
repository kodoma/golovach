package com.kodoma.callback.simple;

/**
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class MyClass implements SomeClass.Callback {

    @Override
    public void callingBack() {
        System.out.println("Вызов метода обратного вызова");
    }
}
