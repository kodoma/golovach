package com.kodoma.callback.simple;

import javax.swing.*;

/**
 * SomeClass class.
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class SomeClass {

    // создаем колбек и его метод
    interface Callback {

        void callingBack();
    }

    private Callback callback;

    public void registerCallBack(Callback callback) {
        this.callback = callback;
    }

    void doSomething() {
        JOptionPane.showMessageDialog(null, "Выполняется работа");
        // вызываем метод обратного вызова
        callback.callingBack();
    }
}