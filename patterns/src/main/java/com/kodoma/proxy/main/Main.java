package com.kodoma.proxy.main;

import com.kodoma.proxy.image.Image;
import com.kodoma.proxy.proxy.ProxyImage;

public class Main {

    public static void main(String[] args) {
        //Клиент не знает, с кем работает
        Image image = new ProxyImage("D:/images/my.jpg");
        image.display();
    }
}
