package com.kodoma.proxy.proxy;

import com.kodoma.proxy.image.Image;
import com.kodoma.proxy.image.RealImage;

/**
 * Proxy позволяет загрузить файл, и только при вызове display() создать реальный объект.
 * Т.е. смысл в разделении сложных задач.
 */
public class ProxyImage implements Image {
    /**
     * VirtualProxy. Значительным преимуществом прокси-объектов данного типа является то, что они позволяют работать с собой как с
     * реальными объектами, откладывая создание последних до того момента, когда это действительно понадобится.
     * Более того, такие прокси-объекты могут даже в какой-то мере заниматься оптимизацией, например, когда и как должен
     * создаваться реальный объект.
     */

    /** Путь к файлу. */
    String file;

    /** Реальный объект - картинка. */
    RealImage image;

    public ProxyImage(String file) {
        this.file = file;
    }

    @Override
    public void display() {
        if (image == null) {
            image = new RealImage(file);
        }
        image.display();
    }
}
