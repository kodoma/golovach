package com.kodoma.proxy.image;

public class RealImage implements Image {

    String file;

    /** При создании картинки приходилось еще загружать файл, даже если при этом метод display() не вызывался. */
    public RealImage(String file) {
        this.file = file;
        load();
    }

    void load() {
        System.out.println(String.format("Загрузка файла %s...", file));
    }

    @Override
    public void display() {
        System.out.println(String.format("Просмотр файла %s...", file));
    }
}
