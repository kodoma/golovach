package com.kodoma.proxy.image;

public interface Image {
    void display();
}
