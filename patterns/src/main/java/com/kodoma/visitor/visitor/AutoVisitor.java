package com.kodoma.visitor.visitor;

import com.kodoma.visitor.auto.Car;
import com.kodoma.visitor.auto.Track;

/**
 * AutoVisitor class.
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class AutoVisitor implements Visitor {

    @Override
    public void visit(Car car) {
        System.out.println("Легковой автомобиль модели: " + car.getModelTitle());
    }

    @Override
    public void visit(Track track) {
        System.out.println("Грузовой автомобиль модели: " + track.getModelTitle());
    }
}
