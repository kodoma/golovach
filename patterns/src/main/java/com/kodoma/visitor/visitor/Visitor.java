package com.kodoma.visitor.visitor;

import com.kodoma.visitor.auto.Car;
import com.kodoma.visitor.auto.Track;

/**
 * Visitor class.
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public interface Visitor {

    void visit(Car car);

    void visit(Track track);
}
