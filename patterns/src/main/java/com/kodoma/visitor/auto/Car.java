package com.kodoma.visitor.auto;

import com.kodoma.visitor.visitor.Visitor;

/**
 * Car class.
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class Car extends Auto {

    public Car(String modelTitle) {
        super(modelTitle);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
