package com.kodoma.visitor.auto;

import com.kodoma.visitor.visitor.Visitor;

/**
 * Auto class.
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public abstract class Auto {

    private String modelTitle;

    public Auto(String modelTitle) {
        this.modelTitle = modelTitle;
    }

    public abstract void accept(Visitor visitor);

    public String getModelTitle() {
        return modelTitle;
    }

    public void setModelTitle(String modelTitle) {
        this.modelTitle = modelTitle;
    }
}
