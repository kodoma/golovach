package com.kodoma.visitor.auto;

import com.kodoma.visitor.visitor.Visitor;

/**
 * Track class.
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class Track extends Auto {

    public Track(String modelTitle) {
        super(modelTitle);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
