package com.kodoma.visitor;

import com.kodoma.visitor.auto.Auto;
import com.kodoma.visitor.auto.Car;
import com.kodoma.visitor.auto.Track;
import com.kodoma.visitor.visitor.AutoVisitor;
import com.kodoma.visitor.visitor.Visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Main class.
 * Created on 12.03.2019.
 * @author Sokolov2-DO.
 */
public class Main {

    public static void main(String[] args) {
        final List<Auto> arr = new ArrayList<>();
        final Visitor visitor = new AutoVisitor();

        arr.add(new Car("ВАЗ"));
        arr.add(new Track("ГАЗель"));
        arr.add(new Car("Mercedes"));
        arr.add(new Track("КамАЗ"));

        for (Auto a : arr) {
            a.accept(visitor);
        }
    }
}
