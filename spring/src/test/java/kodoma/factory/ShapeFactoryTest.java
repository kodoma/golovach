package kodoma.factory;


import com.kodoma.enumeration.ShapeType;
import com.kodoma.factory.ShapeFactory;
import com.kodoma.shapes.Shape;
import com.kodoma.keeper.ShapesKeeper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Тест фабрики
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context/spring-factory-context.xml")
public class ShapeFactoryTest extends AbstractJUnit4SpringContextTests {

    @Autowired private ShapeFactory factory;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void factoryCreateShapesTest() {
        Shape shape = factory.createShape(ShapeType.CIRCLE);
        shape.print();
    }

    @Test
    public void factoryCreateShapesWithKeeperTest() {
        Shape shape = ShapesKeeper.getShape(ShapeType.TRIANGLE);
        shape.print();
    }

}
