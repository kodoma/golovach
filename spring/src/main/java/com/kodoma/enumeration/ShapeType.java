package com.kodoma.enumeration;

/**
 * Тип фигуры
 */
public enum ShapeType {

    TRIANGLE("Triangle"),
    CIRCLE("Circle");

    private String name;

    ShapeType(String name) {
        this.name = name;
    }

}
