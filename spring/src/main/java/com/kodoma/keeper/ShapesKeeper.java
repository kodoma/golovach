package com.kodoma.keeper;

import com.kodoma.enumeration.ShapeType;
import com.kodoma.shapes.Circle;
import com.kodoma.shapes.Shape;
import com.kodoma.shapes.Triangle;

import java.util.HashMap;
import java.util.Map;

/**
 * Мапа фигур
 */
public class ShapesKeeper {

    private static Map<ShapeType, Shape> SHAPES_MAP = new HashMap<>();

    static {
        SHAPES_MAP.put(ShapeType.TRIANGLE, new Triangle());
        SHAPES_MAP.put(ShapeType.CIRCLE, new Circle());
    }

    public static Shape getShape(ShapeType type) {
        return SHAPES_MAP.get(type);
    }

}
