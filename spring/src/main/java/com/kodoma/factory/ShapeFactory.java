package com.kodoma.factory;

import com.kodoma.enumeration.ShapeType;
import com.kodoma.shapes.Shape;

/**
 * Фабрика фигур
 */
public interface ShapeFactory {

    /**
     * Создать фигуру
     * @return Shape
     */
    Shape createShape(ShapeType name);

}
