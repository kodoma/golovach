package com.kodoma.shapes;

import java.io.Serializable;

/**
 * Фигура
 */
public interface Shape extends Serializable {

    /**
     * Распечатать фигуру
     */
    void print();

}
