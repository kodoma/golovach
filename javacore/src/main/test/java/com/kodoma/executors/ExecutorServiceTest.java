package com.kodoma.executors;

import com.kodoma.utils.ThreadUtils;
import com.kodoma.utils.TimeUtils;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.util.concurrent.*;

/**
 * Тест для {@link ExecutorService}.
 * Created on 20.07.2018.
 * @author dmsokol.
 */
public class ExecutorServiceTest {

    private static final Logger LOG = Logger.getLogger(ExecutorServiceTest.class);

    @Test(description = "Создание одного потока-исполнителя")
    public void testStartExecutor() {
        final ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {
            LOG.info(Thread.currentThread().getName() + " Hello!");
            TimeUtils.sleepOneSecond();
        });
        ThreadUtils.shutdownExecutor(executor);
    }

    @Test(description = "Тестирование Callable задачи")
    public void testCallableTask() throws ExecutionException, InterruptedException {
        final ExecutorService executor = Executors.newSingleThreadExecutor();

        final Callable<String> task = () -> {
            TimeUtils.sleep(3);
            return "I all done!";
        };

        final Future future = executor.submit(task);

        while (!future.isDone()) {
            LOG.info("task is process...");
            TimeUtils.sleepOneSecond();
        }
        // Вызов метода get() блокирует поток и ждет завершения задачи, а затем возвращает результат ее выполнения
        LOG.info("task completed, result of task: " + future.get());
        ThreadUtils.shutdownExecutor(executor);
    }

    @Test(description = "Тестирование нескольких потоков")
    public void testFewThreads() throws InterruptedException {
        final ExecutorService executor = Executors.newFixedThreadPool(2);

        final Runnable task = () -> {
            for (int i = 0; i < 10; i++) {
                TimeUtils.sleepHalfSecond();
                LOG.info(Thread.currentThread().getName()  + ": " + i + " zzz");
            }
        };
        executor.execute(task);
        executor.execute(task);
        executor.execute(task);

        executor.awaitTermination(10, TimeUnit.SECONDS);
    }
}
