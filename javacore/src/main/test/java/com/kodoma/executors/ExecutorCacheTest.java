package com.kodoma.executors;

import com.kodoma.utils.TimeUtils;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ExecutorCacheTest class.
 * Created on 20.08.2018.
 * @author dmsokol.
 */
public class ExecutorCacheTest {

    private static final Logger LOG = Logger.getLogger(ExecutorCacheTest.class);

    @Test
    public void test() {
        final ExecutorService executor = Executors.newFixedThreadPool(5);

        final Runnable task = () -> {
            final String threadName = Thread.currentThread().getName();
            ValueHolder.INSTANCE.setValue(threadName, threadName + "Value");
            TimeUtils.sleepOneSecond();
            LOG.info(threadName + ": " + ValueHolder.INSTANCE.getValue(threadName));
        };
        for (int i = 0; i < 5; i++) {
            executor.execute(task);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }
}
