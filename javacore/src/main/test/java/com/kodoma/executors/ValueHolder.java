package com.kodoma.executors;


/**
 * ValueHolder class.
 * Created on 20.08.2018.
 * @author dmsokol.
 */
public class ValueHolder<V> {

    public static final ValueHolder INSTANCE = new ValueHolder();

    private V value;

    private String callId;

    public synchronized V getValue(final String callId) {
        return this.callId.equals(callId) ? value : null;
    }

    public synchronized void setValue(final String callId, final V value) {
       this.callId = callId;
       this.value = value;
    }
}
