package com.kodoma.stream;

import org.apache.commons.lang3.RandomUtils;
import org.testng.collections.Lists;

import java.util.Arrays;
import java.util.List;

/**
 * People class.
 * Created on 16.09.2018.
 * @author Kodoma.
 */
public class People {

    public enum Sex {
        MAN,
        WOMEN
    }

    private String name;
    private Integer age;
    private Sex sex;

    public People(String name, Integer age, Sex sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public Sex getSex() {
        return sex;
    }

    public static List<People> getRandomPeople(final int count) {
        final List<String> menNames = Arrays.asList("Дима", "Алексей", "Илья", "Денис", "Максим", "Саша", "Вася");
        final List<String> womenNames = Arrays.asList("Елена", "Маша", "Оля", "Марина", "Вера", "Аня", "Даша", "Настя");
        final List<People> people = Lists.newArrayList();

        for (int i = 0; i < count; i++) {
            final Sex randomSex = Sex.values()[RandomUtils.nextInt(0, 2)];
            final String randomName = randomSex == Sex.MAN ? menNames.get(RandomUtils.nextInt(0, menNames.size() - 1))
                                                           : womenNames.get(RandomUtils.nextInt(0, womenNames.size() - 1));
            final int randomAge = RandomUtils.nextInt(15, 50);

            people.add(new People(randomName, randomAge, randomSex));
        }
        return people;
    }

    @Override
    public String toString() {
        return "{" +
               "name='" + name + '\'' +
               ", age=" + age +
               ", sex=" + sex +
               '}';
    }

    private Address address;

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public static class Address {
        private City city;

        public City getCity() {
            return city;
        }

        public void setCity(City city) {
            this.city = city;
        }
    }

    public static class City {
        private Street street;

        public Street getStreet() {
            return street;
        }

        public void setStreet(Street street) {
            this.street = street;
        }
    }

    public static class Street {
        private House house;

        public House getHouse() {
            return house;
        }

        public void setHouse(House house) {
            this.house = house;
        }
    }

    public static class House {

        @Override
        public String toString() {
            return "HOME";
        }
    }

    public static AddressBuilder addressBuilder() {
        return new AddressBuilder();
    }

    public static class AddressBuilder {
        private Address address;
        private City city;
        private Street street;
        private House house;

        public AddressBuilder setAddress(final Address address) {
            this.address = address;
            return this;
        }

        public AddressBuilder setCity(final City city) {
            this.city = city;
            return this;
        }

        public AddressBuilder setStreet(final Street street) {
            this.street = street;
            return this;
        }

        public AddressBuilder setHouse(final House house) {
            this.house = house;
            return this;
        }

        public Address build() {
            if (address == null) {
                return new Address();
            }
            address.setCity(city);
            if (city != null) {
                city.setStreet(street);
            }
            if (street != null) {
                street.setHouse(house);
            }
            return address;
        }
    }
}
