package com.kodoma.stream;

import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Action of the Java 8 Stream API.
 * Created on 16.09.2018.
 * @author Kodoma.
 */
public class TestStreamApi {

    private final Logger LOG = Logger.getLogger(TestStreamApi.class);

    @Test(description = "Создание стримов")
    public void creatingStream() throws IOException {

        // 1. Создание стрима из значений
        final Stream<String> streamFromValues = Stream.of("a1", "a2", "a3");

        // 2. Создание стрима из массива
        final String[] array = {"a1", "a2", "a3"};
        final Stream<String> streamFromArrays = Arrays.stream(array);
        final Stream<String> streamFromArrays2 = Stream.of(array);

        // 3. Создание стрима из файла (каждая запись в файле будет отдельной строкой в стриме)
        final Stream<String> streamFromFiles = Files.lines(Paths.get(new File("C:\\Users\\Кодома\\IdeaProjects\\golovach\\javacore\\threads\\src\\main\\java\\com\\kodoma\\finder\\Effective Java.pdf").getAbsolutePath()));

        // 4. Создание стрима из коллекции
        final Collection<String> collection = Arrays.asList("a1", "a2", "a3");
        final Stream<String> streamFromCollection = collection.stream();

        // 5. Создание числового стрима из строки
        final IntStream streamFromString = "123".chars();
        LOG.info("streamFromString = " + Arrays.toString(streamFromString.toArray()));// напечатает streamFromString = 49 , 50 , 51 ,

        // 6. С помощью Stream.builder
        final Stream.Builder<String> builder = Stream.builder();
        final Stream<String> streamFromBuilder = builder.add("a1").add("a2").add("a3").build();

        // 7. Создание бесконечных стримов
        // С помощью Stream.iterate
        final Stream<Integer> streamFromIterate = Stream.iterate(1, n -> n + 2);

        // 8. С помощью Stream.generate
        final Stream<String> streamFromGenerate = Stream.generate(() -> "a1");

        // 9. Создать пустой стрим
        final Stream<String> streamEmpty = Stream.empty();

        // 10.  Создать параллельный стрим из коллекции
        final Stream<String> parallelStream = collection.parallelStream();
    }

    @Test(description = "Примеры использования filter, findFirst, findAny, skip, limit и count")
    public void findTest() {
        final List<String> list = Arrays.asList("a1", "a2", "a3", "a1");

        // Вернуть количество вхождений объекта «a1»
        final long count = list.stream().filter("a1"::equals).count();
        LOG.info("Количество вхождений объекта «a1»: " + count);

        // Вернуть первый элемент коллекции или 0, если коллекция пуста
        final String findFirst = list.stream().findFirst().orElse("0");
        LOG.info("Первый элемент коллекции: " + findFirst);

        // Вернуть последний элемент коллекции или «empty», если коллекция пуста
        final String findLast = list.stream().skip(list.size() - 1).findAny().orElse("empty");
        LOG.info("Последний элемент коллекции: " + findLast);

        // Найти элемент в коллекции равный «a3» или кинуть исключение NoSuchElementException
        final String findA3 = list.stream().filter("a3"::equals).findFirst().get();
        LOG.info("Найден элемент: " + findA3);

        // Вернуть третий элемент коллекции по порядку
        final String findThird = list.stream().skip(2).findFirst().get();
        LOG.info("Третий элемент коллекции: " + findThird);

        // Вернуть два элемента начиная со второго
        final Object[] findTwoStartsFromTwo = list.stream().skip(1).limit(2).toArray();
        LOG.info("Два элемента начиная со второго: " + Arrays.toString(findTwoStartsFromTwo));

        // Выбрать все элементы по шаблону
        final List<String> findContains1 = list.stream().filter(s -> s.contains("1")).collect(Collectors.toList());
        LOG.info("Элементы, содержащие '1': " + findContains1);
    }

    @Test
    public void findPeopleTest() {
        // Выбрать мужчин-военнообязанных (от 18 до 27 лет)
        final List<People> people = People.getRandomPeople(3);
        LOG.info("Люди: " + people);

        final List<People> militaryMen = people.stream().filter(p -> p.getSex() == People.Sex.MAN && p.getAge() >= 18 && p.getAge() < 27).collect(Collectors.toList());
        LOG.info("Военнообязанные мужчины: " + militaryMen);

        // Найти средний возраст среди мужчин
        final double averageMenAge = people.stream().filter(p -> p.getSex() == People.Sex.MAN).mapToInt(People::getAge).average().orElse(0);
        LOG.info("Средний возраст среди мужчин: " + averageMenAge);

        // Найти кол-во потенциально работоспособных людей в выборке (т.е. от 18 лет и учитывая что женщины выходят на пенсию в 60 лет, а мужчина в 65)
        final List<People> workPeople = people.stream().filter(p -> p.getAge() >= 18 && p.getSex() == People.Sex.MAN ?
                                                                    p.getAge() < 65 : p.getAge() < 60).collect(Collectors.toList());
        LOG.info("Работоспособные люди: " + workPeople);
    }

    @Test
    public void distinctTest() {
        final List<String> ordered = Arrays.asList("a1", "a2", "a2", "a3", "a1", "a2", "a2");
        final HashSet<String> nonOrdered = Sets.newHashSet(ordered);

        // Получение коллекции без дубликатов из неупорядоченного стрима
        final List<String> orderedResult = ordered.stream().distinct().collect(Collectors.toList()); // порядок гарантируется
        LOG.info(orderedResult);

        final List<String> nonOrderedResult = nonOrdered.stream().distinct().collect(Collectors.toList()); // порядок не гарантируется
        LOG.info(nonOrderedResult);

        // Если вы используете distinct с классом, у которого переопределен equals, обязательно так же корректно переопределить hashCode
        // в соответствие с контрактом equals/hashCode (самое главное чтобы hashCode для всех equals объектов, возвращал одинаковое значение),
        // иначе distinct может не удалить дубликаты (аналогично, как при использовании HashSet/HashMap),
    }

    @Test
    public void matchTest() {
        final List<String> list = Arrays.asList("a1", "a2", "a3", "a4");

        // Найти существуют ли хоть один «a1» элемент в коллекции
        final boolean isElemExist = list.stream().anyMatch("a1"::equals);
        LOG.info(isElemExist);

        // Найти есть ли символ «1» у всех элементов коллекции
        final boolean isAllElemsContain = list.stream().allMatch(value -> value.contains("1"));
        LOG.info(isAllElemsContain);

        // Проверить что не существуют ни одного «a7» элемента в коллекции
        final boolean isElemNotExist = list.stream().noneMatch("a7"::equals);
        LOG.info(isElemNotExist);
    }

    @Test
    public void mapTest() {
        final List<String> listOne = Arrays.asList("a1", "a2", "a3", "a1");
        final List<String> listTwo = Arrays.asList("1,2,0", "4,5");

        // Добавить "_1" к каждому элементу первой коллекции
        final List<String> concatList = listOne.stream().map(s -> s.concat("_1")).collect(Collectors.toList());
        LOG.info(concatList);

        // В первой коллекции убрать первый символ и вернуть массив чисел (int[])
        final int[] digitList = listOne.stream().mapToInt(s -> Integer.parseInt(s.substring(1))).toArray();
        LOG.info(Arrays.toString(digitList));

        // Из второй коллекции получить все числа, перечисленные через запятую из всех элементов
        final String[] intList = listTwo.stream().flatMap(s -> Arrays.stream(s.split(","))).toArray(String[]::new);
        LOG.info("intList: " + Arrays.toString(intList));

        // Из второй коллекции получить сумму всех чисел, перечисленных через запятую
        final int sum = listTwo.stream().flatMapToInt(p -> Arrays.asList(p.split(",")).stream().mapToInt(Integer::parseInt)).sum();
        LOG.info("sum: " + sum);
    }

    @Test
    public void sortTest() {
        final List<String> list = Arrays.asList("a1", "a4", "a3", "a2", "a1", "a4");
        final List<People> people = People.getRandomPeople(4);

        // Отсортировать коллекцию строк по алфавиту
        final List<String> sortedList = list.stream().sorted().collect(Collectors.toList());
        LOG.info("sortedList: " + sortedList);

        // Отсортировать коллекцию строк по алфавиту в обратном порядке
        final List<String> reverseSortedList = list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        LOG.info("reverseSortedList: " + reverseSortedList);

        // Отсортировать коллекцию строк по алфавиту и убрать дубликаты
        final List<String> sortedDistinctList = list.stream().sorted().distinct().collect(Collectors.toList());
        LOG.info("sortedDistinctList: " + sortedDistinctList);

        // Отсортировать коллекцию строк по алфавиту в обратном порядке и убрать дубликаты
        final List<String> reverseSortedDistinctList = list.stream().sorted(Comparator.reverseOrder()).distinct().collect(Collectors.toList());
        LOG.info("reverseSortedDistinctList: " + reverseSortedDistinctList);

        // Отсортировать коллекцию людей по имени в обратном алфавитном порядке
        final List<People> sortedPeople = people.stream().sorted((o1, o2) -> o2.getName().compareTo(o1.getName())).collect(Collectors.toList());
        LOG.info("sortedPeople: " + sortedPeople);

        // Отсортировать коллекцию людей сначала по полу, а потом по возрасту
        final List<People> sortedSexAgePeople = people.stream().sorted((o1, o2) -> o1.getSex() == o2.getSex() ? o1.getAge().compareTo(o2.getAge())
                                                                                                              : o1.getSex().compareTo(o2.getSex())).collect(Collectors.toList());
        LOG.info("sortedSexAgePeople: " + sortedSexAgePeople);
    }

    @Test
    public void reduceTest() {
        // Метод reduce позволяет выполнять агрегатные функции на всей коллекцией (такие как сумма,
        // нахождение минимального или максимального значение и т.п.), он возвращает одно значение для стрима,
        // функция получает два аргумента — значение полученное на прошлых шагах и текущее значение

        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 2);

        // Получить сумму чисел или вернуть 0
        final Integer sum = list.stream().reduce((s1, s2) -> s1 + s2).orElse(0);

        LOG.info("sum: " + sum);

        // Вернуть максимум или -1
        final Integer max = list.stream().reduce(Integer::max).orElse(-1);

        LOG.info("max: " + max);

        // Вернуть сумму нечетных чисел или 0
        final Integer notOddSum = list.stream().filter(o -> o % 2 != 0).reduce((s1, s2) -> s1 + s2).orElse(0);

        LOG.info("notOddSum: " + notOddSum);
    }
}