package com.kodoma.functional.exceptionhandler;

/**
 * ThrowingFunction class.
 * Created on 28.10.2018.
 * @author Kodoma.
 */
@FunctionalInterface
public interface ThrowingFunction<T, R> extends BaseFunction {

    R applyThrows(T t) throws Exception;

    default R apply(T t) {
        try {
            return applyThrows(t);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}