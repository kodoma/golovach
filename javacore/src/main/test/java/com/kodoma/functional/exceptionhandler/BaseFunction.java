package com.kodoma.functional.exceptionhandler;

/**
 * BaseFunction class.
 * Created on 28.10.2018.
 * @author Kodoma.
 */
public interface BaseFunction {

    static void invoke(ThrowingVoidFunction function) {
        function.apply();
    }

    static <T, R> R invoke(ThrowingFunction<T, R> function) {
        // Можем вызвать с null, т.к. значение перезапишется во время вызова метода applyThrows
        return function.apply(null);
    }
}
