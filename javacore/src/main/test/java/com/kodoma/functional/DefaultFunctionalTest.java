package com.kodoma.functional;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * DefaultFunctionalTest class.
 * Created on 30.11.2018.
 * @author dmsokol.
 */
public class DefaultFunctionalTest {

    private static final Logger LOG = Logger.getLogger(FunctionalTest.class);

    @Test
    public void predicateTest() {
        testPredicateValue(x -> x > 0, 5);
        testPredicateValue(x -> x <= 4, 4);
        testPredicateValue(x -> x == 1, 3);
    }

    @Test
    public void binaryTest() {
        testBinaryValue((a, b) -> a * b, 7L, 8L);
        testBinaryValue((a, b) -> a + b - 10,15L, 3L);
        testBinaryValue((a, b) -> a * a + 2 * a * b + b * b , 2L, 4L);
    }

    @Test
    public void unaryTest() {
        testUnaryValue(s -> s + s, "Kodoma");
        testUnaryValue(String::trim, " Home ");
        testUnaryValue(s -> "((".concat(s).concat("))"), "=^_^=");
    }

    @Test
    public void functionTest() {
        testFunctionValue(Integer::valueOf, "123");
    }

    @Test
    public void functionConsumerTest() {
        testConsumerValue(x -> System.out.printf("Value is %d", x), 10);
    }

    private static void testPredicateValue(final Predicate<Integer> condition, final Integer value) {
        if (condition.test(value)) {
            LOG.info(value + " passed the test");
        } else {
            LOG.info(value + " didn't pass the test");
        }
    }

    private static void testBinaryValue(final BinaryOperator<Long> condition, final Long one, final Long two) {
        LOG.info("The result is: " + condition.apply(one, two));
    }

    private static void testUnaryValue(final UnaryOperator<String> condition, final String value) {
        LOG.info("The result string is: " + condition.apply(value));
    }

    private static void testFunctionValue(final Function<String, Integer> condition, final String value) {
        LOG.info("The result of conversion: " + condition.apply(value));
    }

    private static void testConsumerValue(final Consumer<Integer> condition, final Integer value) {
        condition.accept(value);
    }
}
