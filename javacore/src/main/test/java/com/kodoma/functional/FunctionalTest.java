package com.kodoma.functional;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * FunctionalTest class.
 * Created on 24.10.2018.
 * @author dmsokol.
 */
public class FunctionalTest {

    private final Logger LOG = Logger.getLogger(FunctionalTest.class);

    @Test
    public void functionalTest() {
        filter(n -> n > 7, 5);
        filter(n -> n < 3, 1);
    }

    private void filter(final Func func, final int num) {
        if (func.filter(num)) {
            LOG.info(num);
        }
    }

    private interface Func {

        boolean filter(Integer item);
    }
}