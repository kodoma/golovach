package com.kodoma.functional.exceptionhandler;

import com.sun.istack.internal.NotNull;
import org.junit.Test;

import static com.kodoma.functional.exceptionhandler.BaseFunction.invoke;

/**
 * ThrowingFunctionTest class.
 * Created on 28.10.2018.
 * @author Kodoma.
 */
public class ThrowingFunctionTest {

    @Test
    public void test() {
        final String result = invoke(n -> action(5));
        System.out.println(result);

        final String result2 = invoke(n -> action());
        System.out.println(result2);

        invoke(this::actionVoid);
    }

    public String action(@NotNull final Integer a) {
        return Integer.toString(a);
    }

    public String action() {
        return "action";
    }

    public void actionVoid() {
        System.out.println("void action");
    }
}
