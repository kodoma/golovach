package com.kodoma.functional;

import org.junit.Test;

import java.util.function.Function;

/**
 * FunctionalExTest class.
 * Created on 28.10.2018.
 * @author Kodoma.
 */
public class FunctionalExTest {

    @FunctionalInterface
    public interface ThrowingFunction<T,R> extends Function<T,R> {

        @Override
        default R apply(T t){
            try{
                return applyThrows(t);
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }

        R applyThrows(T t) throws Exception;
    }

    @Test
    public void test() {
    }

    public ThrowingFunction handleEx(ThrowingFunction function) {
        return null;
    }

    public ThrowingFunction action() throws Exception {
        return null;
    }







    @FunctionalInterface
    public interface FunctionWithException<T, R, E extends Exception> {

        R apply(T t) throws E;
    }

    private <T, R, E extends Exception> Function<T, R> wrapper(FunctionWithException<T, R, E> fe) {
        return arg -> {
            try {
                return fe.apply(arg);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}
