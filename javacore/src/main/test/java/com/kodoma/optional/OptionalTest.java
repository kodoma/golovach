package com.kodoma.optional;

import com.kodoma.stream.People;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.util.Optional;

/**
 * OptionalTest class.
 * Created on 16.09.2018.
 * @author Kodoma.
 */
public class OptionalTest {

    private final Logger LOG = Logger.getLogger(OptionalTest.class);

    @Test
    public void optionalTest() {
        final People people = new People("a", null, null);
        final Optional<People> pop = Optional.of(people);

        people.setAddress(People.addressBuilder()
                                .setAddress(new People.Address())
                                .setCity(new People.City())
                                .setStreet(new People.Street())
                                .setHouse(new People.House()).build());

        final Optional<People.House> house = pop.map(People::getAddress)
                                                .map(People.Address::getCity)
                                                .map(People.City::getStreet)
                                                .map(People.Street::getHouse);

        LOG.info(house.isPresent() ? house.get() : "House not found");
    }
}
