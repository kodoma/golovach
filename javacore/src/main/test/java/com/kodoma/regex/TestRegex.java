package com.kodoma.regex;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TestRegex class.
 * Created on 05.10.2018.
 * @author dmsokol.
 */
public class TestRegex {

    private final Logger LOG = Logger.getLogger(TestRegex.class);

    @Test
    public void testFile() throws IOException {
        final String sdp = "v=0\n"
                           + "o=- 0 0 IN IP4 192.168.100.10\n"
                           + "s=session\n"
                           + "c=IN IP4 192.168.100.10\n"
                           + "b=CT:99980\n"
                           + "t=0 0\n"
                           + "m=applicationsharing 31122 TCP/RTP/AVP 127\n"
                           + "a=ice-ufrag:S7iq\n"
                           + "a=ice-pwd:lfWR9Q6dUCGYPX0Qj2z5YmLW\n"
                           + "a=candidate:1 1 TCP-PASS 2120613887 192.168.100.10 6831 typ host\n";

        System.out.println(getCandidatePassAttribute(sdp));

    }

    private static String getCandidatePassAttribute(String sdp) {
        Pattern p = Pattern.compile("a=candidate:1.*\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3} \\d{1,5}");
        Matcher m = p.matcher(sdp);
        if (m.find()) {
            String result = m.group();
            String[] mass = result.split(" ");
            String port = mass[mass.length-1];
            String ip = mass[mass.length-2];
            return ip + ":" + port;
        }
        return null;
    }

    static String getAttribute(String field, String sdp) {
        Pattern p = Pattern.compile(field + ".*");
        Matcher m = p.matcher(sdp);
        if (m.find()) {
            String result = m.group();
            int length = result.length();
            return result.substring(field.length(), length);
        }
        return null;
    }

    private String getUserId(final String remoteAddress, final String callId) {
        final Pattern pattern = Pattern.compile("sip:(.*?)@");
        final Matcher matcher = pattern.matcher(remoteAddress);
        Exception exception = null;
        if (matcher.find()) {
            try {
                return matcher.group(1);
            } catch (IllegalStateException | IndexOutOfBoundsException e) {
                exception = e;
            }
        }
        LOG.error(exception != null ? exception : "");
        return null;
    }
}
