package com.kodoma.scheduler;

import org.apache.log4j.Logger;
import com.kodoma.utils.TimeUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Тест для {@link ScheduledExecutorService}.
 * Created on 20.07.2018.
 * @author dmsokol.
 */
public class ScheduledExecutorServiceTest {

    private final Logger LOG = Logger.getLogger(ScheduledExecutorServiceTest.class);

    public static void main(String[] args) {
        new ScheduledExecutorServiceTest().test();
    }

    /**
     * Для того, чтобы периодически запускать задачу, мы можем использовать пул потоков с планировщиком.
     * ScheduledExecutorService способен запускать задачи один или несколько раз с заданным интервалом.
     */
    public void test() {
        final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        final Runnable task = () -> LOG.info("doing task...");

        // Задача начнет выполняться через 3 секунды
        final ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);

        while (!future.isDone()) {
            LOG.info("time left " + future.getDelay(TimeUnit.SECONDS));
            TimeUtils.sleepOneSecond();
        }
        executor.shutdown();
        LOG.info("task is finished");
    }
}
