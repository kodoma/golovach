package com.kodoma.utils;

import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Инструменты для управления потоками.
 * Created on 20.07.2018.
 * @author dmsokol.
 */
public class ThreadUtils {

    private static final Logger LOG = Logger.getLogger(ThreadUtils.class);

    public static void shutdownExecutor(final ExecutorService executor) {
        try {
            // Ждем завершения запущенных задач
            executor.shutdown();
            // Ожидаем завершения запущенных задач в течение 5 секунд
            executor.awaitTermination(5, TimeUnit.SECONDS);

        } catch (InterruptedException e) {
            LOG.error("tasks were interrupted");
        } finally {
            if (!executor.isTerminated()) {
                LOG.error("cancel non-finished tasks");
            }
            executor.shutdownNow();
            LOG.info("executor shutdown");
        }
    }
}
