package com.kodoma.utils;

/**
 * Инструменты для печати объектов.
 * Created on 27.06.2018.
 * @author Упя.
 */
public class PrintUtils {

    public static void println(final Object o) {
        System.out.println(o);
    }

    public static void print(final Object o) {
        System.out.print(o + " ");
    }
}
