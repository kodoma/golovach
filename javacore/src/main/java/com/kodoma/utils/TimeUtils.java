package com.kodoma.utils;

import java.util.concurrent.TimeUnit;

/**
 * Инструменты для управления временем потоков.
 * Created on 27.06.2018.
 * @author Упя.
 */
public class TimeUtils {

    /** Заснуть на пол секунды. */
    public static void sleepHalfSecond() {
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /** Заснуть на одну секунду. */
    public static void sleepOneSecond() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep(final long seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
