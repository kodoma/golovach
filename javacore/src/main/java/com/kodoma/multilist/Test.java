package com.kodoma.multilist;

import com.google.common.collect.Lists;
import com.kodoma.utils.PrintUtils;
import com.kodoma.utils.TimeUtils;

import java.util.List;

/**
 * Чтение списка чисел несколькими потоками.
 * Created on 27.06.2018.
 * @author Упя.
 */
public class Test {

    public static void main(String[] args) {
        new Test().testThreadList();
    }

    public void testThreadList() {
        final MyList myList = new MyList(34);

        TimeUtils.sleepHalfSecond();

        for (int i = 0; i < 5; i++) {
            new Thread() {

                @Override
                public void run() {
                    myList.get();
                }
            }.start();
        }
    }

    public class MyList {

        private volatile List<Integer> list;

        private volatile int[] sectors = new int[10];

        final Object monitor = new Object();

        public MyList(int size) {
            fillList(size);
        }

        public void get() {
            new Thread(this::checkThread).start();

            while (true) {
                final int sectorLength = list.size() / 10;
                int sectorNumber = 0;
                int end;

                for (int i = 0; i < sectors.length; i++) {
                    if (sectors[i] == 0) {
                        sectorNumber = i;
                        sectors[i] = 1;
                        break;
                    }
                }
                //PrintUtils.println(Thread.currentThread().getName() + ": " + Arrays.toString(sectors));
                end = sectorLength * sectorNumber + sectorLength;

                if (sectors[sectors.length - 1] == 1) {
                    synchronized (monitor) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    end += list.size() % 10;
                    //PrintUtils.println("END: " + end);
                }
                PrintUtils.println(Thread.currentThread().getName() + " TIMES: " + (end - sectorLength * sectorNumber));

                if (list.get(list.size() - 1) == 0) {
                    break;
                }
                for (int i = sectorLength * sectorNumber; i < end; i++) {
                    list.set(i, 0);
                    PrintUtils.println(this);
                    TimeUtils.sleepHalfSecond();
                }
            }
        }

        public void checkThread() {
            synchronized (monitor) {
                while (true) {
                    if (sectors[sectors.length - 1] == 1) {
                        monitor.notify();
                        break;
                    }
                }
            }
        }

        private void fillList(final int size) {
            list = Lists.newArrayList();
            for (int i = 0; i < size; i++) {
                list.add(i);
            }
        }

        private int numberLength(int num) {
            int count = num == 0 ? 1 : 0;
            while (num != 0) {
                count++;
                num /= 10;
            }
            return count;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();

            sb.append("[");
            sb.append(list.get(0));

            for (int i = 1; i < list.size(); i++) {
                if (numberLength(list.get(i)) == 1) {
                    sb.append(" ");
                }
                sb.append(", ").append(list.get(i));
            }
            sb.append("]");
            return sb.toString();
        }
    }
}
