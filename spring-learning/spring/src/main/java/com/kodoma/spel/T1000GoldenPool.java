package com.kodoma.spel;

import com.kodoma.collections.RobotPool;
import com.kodoma.robotdata.interfaces.Robot;

import java.util.Collection;

public class T1000GoldenPool implements RobotPool {

    private Collection<Robot> robotCollection;

    public T1000GoldenPool(Collection<Robot> robotCollection) {
        super();
        this.robotCollection = robotCollection;
    }

    @Override
    public Collection<Robot> getRobotCollection() {
        return robotCollection;
    }

    public void beginShow() {
        for (Robot robot : robotCollection) {
            robot.action();
            System.out.println();
        }
    }
}
