package com.kodoma.collections;

import com.kodoma.robotdata.interfaces.Robot;

import java.util.Collection;
import java.util.Map;

public class T1000MapPool implements RobotPool {

    private Map<String, Robot> robotCollection;

    public T1000MapPool(Map<String, Robot> robotCollection) {
        this.robotCollection = robotCollection;
    }

    @Override
    public Collection<Robot> getRobotCollection() {
        return robotCollection.values();
    }

    public void action() {
        for (Map.Entry<String, Robot> map : robotCollection.entrySet()) {
            System.out.println("Color: " + map.getKey());
            final Robot robot = map.getValue();
            robot.action();
            System.out.println();
        }
    }
}
