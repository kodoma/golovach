package com.kodoma.collections;

import com.kodoma.robotdata.interfaces.Robot;

import java.util.Collection;

public interface RobotPool {

    Collection<Robot> getRobotCollection();

}
