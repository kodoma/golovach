package com.kodoma.customqualifier;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Run {

    public static void main(String... args) {
        final ApplicationContext xmlContext = new ClassPathXmlApplicationContext("context/customqualifier/qualifier-context.xml");
        final Shepperd shepperd = xmlContext.getBean(Shepperd.class);

        System.out.println(shepperd);
    }
}
