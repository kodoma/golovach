package com.kodoma.customqualifier;

import com.kodoma.customqualifier.annotations.DogType;
import org.springframework.stereotype.Component;

@Component
@DogType
public class Dog extends Animal {

    @Override
    public String toString() {
        return "Dog {}";
    }
}
