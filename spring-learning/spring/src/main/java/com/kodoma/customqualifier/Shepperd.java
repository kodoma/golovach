package com.kodoma.customqualifier;

import com.kodoma.customqualifier.annotations.CatType;
import com.kodoma.customqualifier.annotations.DogType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Shepperd {

    @Autowired
    private @DogType Animal dog;

    @Autowired
    private @CatType Animal cat;

    @Override
    public String toString() {
        return "Shepperd {" +
                "dog=" + dog +
                ", cat=" + cat +
                '}';
    }
}
