package com.kodoma.customqualifier;

import com.kodoma.customqualifier.annotations.CatType;
import org.springframework.stereotype.Component;

@Component
@CatType
public class Cat extends Animal {

    @Override
    public String toString() {
        return "Cat {}";
    }
}
