package com.kodoma.robotdata.robot;

import com.kodoma.robotdata.interfaces.Hand;
import com.kodoma.robotdata.interfaces.Head;
import com.kodoma.robotdata.interfaces.Leg;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class ModelX1000 extends BaseModel implements InitializingBean, DisposableBean {

    private String color;
    private int year;
    private boolean soundEnabled;

    public ModelX1000() {
        super();
        System.out.println(this + " ModelX1000 constructor");
    }

    public ModelX1000(Hand hand, Leg leg, Head head, String color, int year, boolean soundEnabled) {
        super(hand, leg, head);
        this.color = color;
        this.year = year;
        this.soundEnabled = soundEnabled;
        System.out.println(this + " ModelX1000 constructor");
    }

    @Override
    public void action() {
        head.calc();
        hand.catchSomething();
        leg.go();
        getProperties();
    }

    @Override
    public void dance() {
        System.out.println("T1000 is dancing!");
    }

    public void getProperties() {
        System.out.println("color: " + color);
        System.out.println("year: " + year);
        System.out.println("sound enabled: " + soundEnabled);
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy() ModelX1000");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("create() ModelX1000");
    }
}
