package com.kodoma.robotdata.robot.autowiring;

import com.kodoma.robotdata.interfaces.Hand;
import com.kodoma.robotdata.interfaces.Head;
import com.kodoma.robotdata.interfaces.Leg;

public class ModelT1000v2 extends BaseModelV2 {

	public String color;
	private int year;
	private boolean soundEnabled;

	public ModelT1000v2() {
	}

	public ModelT1000v2(Hand hand, Leg leg, Head head) {
		super();
		this.hand = hand;
		this.leg = leg;
		this.head = head;
	}

    public ModelT1000v2(String color, int year, boolean soundEnabled) {
        this.color = color;
        this.year = year;
        this.soundEnabled = soundEnabled;
    }

    public ModelT1000v2(Hand hand, Leg leg, Head head, String color, int year, boolean soundEnabled) {
        this.hand = hand;
        this.leg = leg;
        this.head = head;
        this.color = color;
        this.year = year;
        this.soundEnabled = soundEnabled;
    }

    @Override
	public void action() {
		head.calc();
		hand.catchSomething();
		leg.go();
		getProperties();
	}

	@Override
	public void dance() {
		System.out.println("T1000 is dancing!");
	}

	public void getProperties() {
        System.out.println("color: " + color);
        System.out.println("year: " + year);
        System.out.println("sound enabled: " + soundEnabled);
    }

    /**
     * Обработка самим объектом.
     */
    public void initObject() {
        System.out.println("init: {" + this + ", " + hand + ", " + leg + ", " + head + "}");
    }

    /**
     * Обработка самим объектом.
     */
    public void destroyObject() {
        System.out.println("destroy");
    }

    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Leg getLeg() {
        return leg;
    }

    public void setLeg(Leg leg) {
        this.leg = leg;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }
}
