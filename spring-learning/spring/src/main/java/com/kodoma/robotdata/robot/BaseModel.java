package com.kodoma.robotdata.robot;

import com.kodoma.robotdata.interfaces.Hand;
import com.kodoma.robotdata.interfaces.Head;
import com.kodoma.robotdata.interfaces.Leg;
import com.kodoma.robotdata.interfaces.Robot;

/**
 * Базовая модель робота.
 */
public abstract class BaseModel implements Robot {

    protected Hand hand;

    protected Leg leg;

    protected Head head;

    public BaseModel() {
        System.out.println(this + " BaseModel constructor()");
    }

    public BaseModel(Hand hand, Leg leg, Head head) {
        this.hand = hand;
        this.leg = leg;
        this.head = head;
        System.out.println(this + " BaseModel constructor()");
    }

    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Leg getLeg() {
        return leg;
    }

    public void setLeg(Leg leg) {
        this.leg = leg;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }
}
