package com.kodoma.robotdata.toshiba;

import com.kodoma.robotdata.interfaces.Hand;


public class ToshibaHand implements Hand {
	
	public void catchSomething(){
		System.out.println("Catched from Toshiba!");
	}

}
