package com.kodoma.robotdata.sony;

import com.kodoma.robotdata.interfaces.Leg;


public class SonyLeg implements Leg {
	
	public void go() {
		System.out.println("Go to Sony!");
	}

}
