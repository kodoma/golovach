package com.kodoma.robotdata.sony;

import com.kodoma.robotdata.interfaces.Head;


public class SonyHead implements Head {
	
	public void calc() {
		System.out.println("Thinking about Sony...");
	}

}
