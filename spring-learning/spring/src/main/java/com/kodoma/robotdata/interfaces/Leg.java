package com.kodoma.robotdata.interfaces;

public interface Leg {
	
	void go();
}
