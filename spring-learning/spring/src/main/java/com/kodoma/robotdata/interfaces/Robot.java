package com.kodoma.robotdata.interfaces;

public interface Robot {

    void action();

    void dance();
}
