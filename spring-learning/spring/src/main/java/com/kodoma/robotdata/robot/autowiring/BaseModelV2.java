package com.kodoma.robotdata.robot.autowiring;

import com.kodoma.robotdata.interfaces.Hand;
import com.kodoma.robotdata.interfaces.Head;
import com.kodoma.robotdata.interfaces.Leg;
import com.kodoma.robotdata.interfaces.Robot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Базовая модель робота.
 */
public abstract class BaseModelV2 implements Robot {

    @Autowired// @Autowired(required = false) - поле не обязательно заполнять
    // По-умолчанию @Autowired ищет бины по типу.
    // Значения могут автоматически заполняться даже если нет конструктора.
    protected Hand hand;

    @Autowired
    protected Leg leg;

    @Autowired
    @Qualifier("GoldHead")
    protected Head head;

    public BaseModelV2() {
        System.out.println(this + " BaseModel constructor()");
    }

    public BaseModelV2(Hand hand, Leg leg, Head head) {
        this.hand = hand;
        this.leg = leg;
        this.head = head;
        System.out.println(this + " BaseModel constructor()");
    }

    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Leg getLeg() {
        return leg;
    }

    public void setLeg(Leg leg) {
        this.leg = leg;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }
}
