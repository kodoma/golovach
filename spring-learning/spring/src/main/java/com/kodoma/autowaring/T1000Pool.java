package com.kodoma.autowaring;

import com.kodoma.collections.RobotPool;
import com.kodoma.robotdata.interfaces.Robot;

import java.util.List;

public class T1000Pool implements RobotPool {

    private List<Robot> robotCollection;

    public T1000Pool(List<Robot> robotCollection) {
        this.robotCollection = robotCollection;
    }

    @Override
    public List<Robot> getRobotCollection() {
        return robotCollection;
    }

    public void setRobotCollection(List<Robot> robotCollection) {
        this.robotCollection = robotCollection;
    }

    public void action() {
        for (Robot robot : robotCollection) {
            robot.action();
            System.out.println();
        }
    }
}
