package com.kodoma.robotconveyor;

import com.kodoma.robotdata.interfaces.Robot;

public interface RobotConveyor {

    Robot createRobot();
}
