package com.kodoma.robotconveyor;

import com.kodoma.robotdata.interfaces.Robot;

public abstract class T1000Conveyor implements RobotConveyor {

    @Override
    public abstract Robot createRobot();
}
