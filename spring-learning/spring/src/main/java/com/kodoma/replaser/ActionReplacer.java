package com.kodoma.replaser;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;

public class ActionReplacer implements MethodReplacer {

    /**
     *
     * @param o - объект для замещения
     * @param method - метод для замещения
     * @param objects - параметры
     * @return
     * @throws Throwable
     */
    @Override
    public Object reimplement(Object o, Method method, Object[] objects) throws Throwable {
        System.out.println("new action!!!");
        return null;
    }
}
