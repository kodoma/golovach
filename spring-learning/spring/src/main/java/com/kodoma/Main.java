package com.kodoma;


import com.kodoma.robotdata.robot.autowiring.ModelT1000v2;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context/annotations-context.xml");

/*        RobotConveyor t1000Conveyor = (RobotConveyor)context.getBean("t1000Conveyor");
        System.out.println("terminator 1: " + t1000Conveyor.createRobot());
        System.out.println("terminator 2: " + t1000Conveyor.createRobot());
        System.out.println("terminator 3: " + t1000Conveyor.createRobot());*/

        final Object obj = context.getBean("t1000");

        if (obj instanceof ModelT1000v2) {
            ModelT1000v2 t1000 = (ModelT1000v2)obj;
            t1000.action();
        }
    }
}
