package verify_arg;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 *
 */
public class SimpleMock {

    List<String> list;

    @Before
    public void setUp() {
        this.list = new ArrayList<>();
    }

    @Test
    public void test_addAll() {
        Collection<String> arg = mock(Collection.class);

        when(arg.toArray()).thenReturn(new String[] {"A", "B", "C"});

        list.addAll(arg);
        list.addAll(arg);
        list.addAll(arg);

        assertEquals(list, Arrays.asList("A", "B", "C", "A", "B", "C", "A", "B", "C"));
        verify(arg, times(3)).toArray();
        verifyNoMoreInteractions(arg);
    }

}
