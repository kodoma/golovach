package then_throw;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class SimpleMock {

    public static void main(String[] args) {
        List<String> list = mock(List.class);

        when(list.get(eq(8))).thenThrow(new RuntimeException("Блэт!"));

        for (int k = 0; k < 10; k++) {
            System.out.println("list.get(" + k + "): " + list.get(k));
        }
    }

}
