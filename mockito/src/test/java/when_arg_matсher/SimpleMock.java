package when_arg_matсher;

import org.mockito.ArgumentMatcher;

import java.util.List;

import static org.mockito.ArgumentMatchers.intThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class SimpleMock {

    public static void main(String[] args) {
        List<String> list = mock(List.class);

        ArgumentMatcher<Integer> matcher = new ArgumentMatcher<Integer>() {
            @Override
            public boolean matches(Integer integer) {
                return integer % 3 == 0;
            }
        };
        when(list.get(intThat(matcher))).thenReturn("Hello!");

        for (int k = 0; k < 10; k++) {
            System.out.println("list.get(" + k + "): " + list.get(k));
        }
    }

}
