package mock_types;

import org.mockito.Mockito;

import java.util.List;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;

/**
 *
 */
public class SimpleMock {

    public static void main(String[] args) {
        //Программирование дефолтного
        //Мок возвращает ненулевые значение при вызове своих методов
        List listDeepStub = mock(List.class, Mockito.withSettings().defaultAnswer(RETURNS_DEEP_STUBS));
        System.out.println(listDeepStub.size());
        System.out.println(listDeepStub.isEmpty());
        System.out.println(listDeepStub.iterator());
        System.out.println(listDeepStub.iterator().hasNext());
        System.out.println(listDeepStub.iterator().next());
        System.out.println(listDeepStub.subList(0, 10).get(0).toString());
    }

}
